<?php
// https://gitlab.com/azman1204/phpintro
https://codeshare.io/5eAK1l
echo "Hello World";
// variable
$nama = "azman";
$bil = 10;
echo "<br>";
echo "Nama saya ialah $nama dan mempunyai $bil anak";

// array. 1. normal, 2. associative
// normal. index bermula dari 0
$name = ['Abu', 'Ali', 'Chong', 'Ramasamy'];
echo '<br> Nama kawan saya ialah ' . $name[3]; 

// assoctiave array. key => value
$umur = ['abu' => 30, 'ali' => 35, 'chong' => 43, 'ramasamy' => 55];
echo "Umur ali ialah {$umur['ali']}";
echo "Umur ali ialah " . $umur['ali'];

// object oriented programming (OOP)
// class vs object
class Rumah {
    var $bilik = 3;
    var $harga = 100000;
}

class Kereta {
    var $warna = 'merah'; // property
    var $gear = 'P';

    // method
    function tukarGear($no) {
        $this->gear = $no;
    }
}

$toyota = new Kereta(); // create a new object
echo "Kereta ini bewarna " . $toyota->warna;
$toyota->warna = 'Biru';
echo "Warna baru kereta ini ialah " . $toyota->warna;
echo "gear = " . $toyota->gear;
$toyota->tukarGear('D');
echo "gear = " . $toyota->gear;

// inheritance / pewarisan, child inherits all from parent except private stuff
class MPV extends Kereta {
    var $sunRoof = true;
}

$x50 = new MPV();
echo "Warna X50 ialah " . $x50->warna; 
?>